
import requests
import json
from datetime import datetime
from time import sleep
from google.protobuf.json_format import MessageToDict
from google.transit import gtfs_realtime_pb2
from google.protobuf.json_format import MessageToJson

from datetime import date, datetime
from time import mktime

from rx.operators import timestamp

now = datetime.now().timestamp()
# print(datetime.fromtimestamp(now))

#set a rule to start at 19 June 2021 at 12:00:00 AM
print("Code started at: ", datetime.now(), "There's a wait till 19 June 2021 at 12:00:00 AM")
print("Sending requets to the API started at:", datetime.now())

APIs = ['IZjFu6LJDgEJ6b0wqKj9','aCaEZutHoyTz0kkGGh77', 'JUp457WI2xXlrxk0YrFL','ow1SIMs32a9lbyP6WQgv', 'oF6itkxvEKTc6xRDA9Dv', 'jE1KxH0LHxVdOEzG8e1G','P6L02EApifRPTGW6C9RU',
        'qqpqedgISACX47MRr38N', 'PazSzKxDJCu2OntqZonS']
#Collect data for 12 weeks every 30 seconds

feed = gtfs_realtime_pb2.FeedMessage()


def send_request():
    response = requests.get("https://gtfs.translink.ca/v2/gtfsalerts?apikey={}".format(APIs[2]))
    feed.ParseFromString(response.content)
    for ent in feed.entity:
        # print(ent.alert.informed_entity[0].agency_id)
        
        alerts_dict = {
            "measurment" : "TranslinkAlerts",
            "tags" : {
                "id" : ent.id,
                "agency_id" : ent.alert.informed_entity[0].agency_id,
                "route_id" : ent.alert.informed_entity[0].route_id,
                "cause" : ent.alert.cause,
                "header_text" : ent.alert.header_text.translation[0].text,
                "description_text" : ent.alert.description_text.translation[0].text
            },
            "fields" : {
                "route_type" : ent.alert.informed_entity[0].route_type,
                "severity_level" : ent.alert.severity_level
            },
            "time" : ent.alert.active_period[0].start,

        }
        print(alerts_dict)
        import sys; sys.exit();

    
    # for ent in feed.entity:
    #     = ent)
    #     sleep(15)
    #     # if i <= 10:
    #     trip_id = ent.trip_update.trip.trip_id
    #     route_id = ent.trip_update.trip.route_id
    #     direction_id = ent.trip_update.trip.direction_id
        
    #     vehicle_id = ent.trip_update.vehicle.id
    #     vehicle_label = ent.trip_update.vehicle.label
        
    #     for stop in ent.trip_update.stop_time_update:
    #         stop_id = stop.stop_id
    #         stop_sequence = stop.stop_sequence
    #         arrival_delay = stop.arrival.delay
    #         # arrival_time = datetime.strftime('%Y-%m-%d %H:%M:%S.%f', time.localtime(stop.arrival.time))
    #         # arrival_time = datetime.fromtimestamp(stop.arrival.time).strftime('%Y-%m-%d %H:%M:%S.%f')
    #         arrival_time = stop.arrival.time
    #         departure_delay = stop.departure.delay
    #         departure_time = stop.departure.time
        #     # print([trip_id,route_id,vehicle_id,vehicle_label,stop_id,stop_sequence,arrival_time, arrival_delay, departure_time, departure_delay])
        #     sequence.append([trip_id,route_id,vehicle_id,vehicle_label,stop_id,stop_sequence,arrival_time, arrival_delay, departure_time, departure_delay])

    # TripUpdate = pd.DataFrame(sequence,  columns=['trip_id','route_id','vehicle_id','vehicle_label','stop_id','stop_sequence','arrival_time', 'arrival_delay', 'departure_time', 'departure_delay'])
    # TripUpdate['trip_id'] = TripUpdate['trip_id'].astype(str)
    # TripUpdate['route_id'] = TripUpdate['route_id'].astype(str)
    # TripUpdate['vehicle_id'] = TripUpdate['vehicle_id'].astype(str)
    # TripUpdate['vehicle_label'] = TripUpdate['vehicle_label'].astype(str)
    # TripUpdate['stop_id'] = TripUpdate['stop_id'].astype(str)

    # TripUpdate.to_csv(r'D:\CV\Modeler-Translink\GTFSANALYSIS\data_30sec\TripUpdate-Translink.csv', index=False)

        # with open('/home/temp/HexMap/data-prep-realtime-GTFS/data/TripUpdate-{}.pb'.format(now), 'wb') as file:
        #     file.write(response.content)

        # response = requests.get("https://gtfs.translink.ca/v2/gtfsposition?apikey={}".format(APIs[1]))
        # with open('/home/temp/HexMap/data-prep-realtime-GTFS/data/PositionUpdate-{}.pb'.format(now), 'wb') as file:
        #     file.write(response.content)
            
        # response = requests.get("https://gtfs.translink.ca/v2/gtfsalerts?apikey={}".format(APIs[2]))

        # with open('/home/temp/HexMap/data-prep-realtime-GTFS/data/ServiceAlerts-{}.pb'.format(now), 'wb') as file:
        #     file.write(response.content)
    # except:
    #     print("Failed for API")
    #     sleep(600)


def run():
    send_request()

if __name__ == "__main__":
    run()