#!/usr/bin/env python
# Ali Yazdizadeh, Kyle Fitzsimmons, 2021
import psycopg2
import config
from psycopg2 import sql
import pandas as pd
# database connection
conn = psycopg2.connect(host=config.DATABASE_HOST,
                        port=config.DATABASE_PORT,
                        user=config.DATABASE_USER,
                        password=config.DATABASE_PASSWORD,
                        dbname=config.DATABASE_DBNAME)

cur = conn.cursor()

col_names={
    'agency'                    :['agency_url','agency_name','agency_timezone','agency_id','agency_lang'],
    'calendar'                  :['service_id','start_date','end_date','monday','tuesday','wednesday','thursday','friday','saturday','sunday'],
    'calendar_dates'            :['service_id','date','exception_type'],
    'routes'                    :['route_long_name','route_type','route_text_color','route_color','agency_id','route_id','route_url','route_desc','route_short_name'],
    'shapes'                    :['shape_id','shape_pt_lat','shape_pt_lon','shape_pt_sequence','shape_dist_traveled'],
    'stop_times'                :['trip_id','arrival_time','departure_time','stop_id','stop_sequence','stop_headsign','pickup_type','drop_off_type','shape_dist_traveled','timepoint'],
    'stops'                     :['stop_lat','stop_code','stop_lon','stop_id','stop_url','parent_station','stop_desc','stop_name','location_type','zone_id'],
    'trips'                     :['block_id','bikes_allowed','route_id','wheelchair_accessible','direction_id','trip_headsign','shape_id','service_id','trip_id','trip_short_name'],
    'stop_order_exceptions'     :['route_name','direction_name','direction_do','stop_id','stop_name','stop_do'],
    'directions'                :['direction','direction_id','route_id','route_short_name'],
    'direction_names_exceptions':['route_name','direction_id','direction_name','direction_do']
}

file_names  = ['agency','calendar','calendar_dates',  'routes', 'shapes', 'stop_times',   'stops',   'trips', 
'stop_order_exceptions', 'directions', 'direction_names_exceptions']

table_names = ['gtfs_translink_agency', 'gtfs_translink_calendar', 'gtfs_translink_calendar_dates', 
    'gtfs_translink_routes', 'gtfs_translink_shapes', 'gtfs_translink_stop_times', 'gtfs_translink_stops', 'gtfs_translink_trips', 
    'gtfs_translink_stop_order_exceptions', 'gtfs_translink_directions', 'gtfs_translink_direction_names_exceptions']

def copy_to_database():
    for idx, file_name in enumerate(file_names):
        cur.execute(sql.SQL("COPY {table}({fields}) FROM '/data/{fileName}.txt' DELIMITER ',' CSV HEADER").format(
            table=sql.Identifier(table_names[idx]),
            fields=sql.SQL(',').join([sql.Identifier(col) for col in col_names[file_name]]),
            fileName=sql.Identifier(file_name)).as_string(conn).replace('"', "")
        )
        conn.commit()

def run():
    copy_to_database()
    # copy_()

if __name__ == '__main__':
    run()


