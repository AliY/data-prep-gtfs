#### The repo itends to:
- Set up the PostgreSQL and InfluxDB databases.
- Create db in PostgreSQL to save Translink GTFS data.
- Send requests to Translink realtime-GTFS API and receives the realtime GTFS (TripUpdate, VehiclePosition & Alerts)
- Write the realtime GTFS data into the InfluxDB tables


##### Step 1. Run the docker-compose file

(If already any influxdb, grafana or telegraph containers on the system change the name of the new containers in each of the related   `$*.env` files or remove the existing containers on the system with `$sudo docker rm influxdb telegraf grafana` .)

- `$ sudo docker-compose up -d`

##### Step 2. create the venv
 - `$ python -m venv prep-venv`
 - `$ . prep-venv/bin/activate`
 - `$ pip install -r requirements.txt `


##### Step 3. Create PostgreSQL database for GTFS data and data to the tables

- `$python create_postgresql_database.py`
- `$python copy_GTFS_to_tables.py`


##### Step 4. Send request to the transit agency API, receive realtime GTFS data and write it to InfluxDB

- Open the url in the browser: [http://localhost:8086/] to access the Telegraf dashboard
- Click on **_Get Started_**
- Choose a _username, password, organization name and a bucket name_ and press **_Continue_**
- On the next page, click on **_Quick Start_**, and then, **_Load Data_** .
- On the next page (i.e. Load Data), search your desired language inside the search bar
- Copy the InfluxDB database token and the credentials. For python, an example is:
    - `$token = "\\token string\\"`
    - `$org = "HexMap"`
    - `$bucket = "realtimeGTFS"`
- Paste the copied credential inside the `$write_realtime_GTFS_to_influx.py` file and run  `$ python write_realtime_GTFS_to_influx.py` file



