#!/usr/bin/env python
# Ali Yazdizadeh, Kyle Fitzsimmons, 2021
import psycopg2
import config
from psycopg2 import sql

# database connection
conn = psycopg2.connect(host=config.DATABASE_HOST,
                        port=config.DATABASE_PORT,
                        user=config.DATABASE_USER,
                        password=config.DATABASE_PASSWORD,
                        dbname=config.DATABASE_DBNAME)

cur = conn.cursor()



def drop_tables():
    table_names = ['gtfs_translink_agency', 'gtfs_translink_calendar', 'gtfs_translink_calendar_dates', 
    'gtfs_translink_routes', 'gtfs_translink_shapes', 'gtfs_translink_stop_times', 'gtfs_translink_stops', 'gtfs_translink_trips', 'gtfs_translink_stop_order_exceptions', 
    'gtfs_translink_directions', 'gtfs_translink_direction_names_exceptions']

    for tbl_query in table_names: 
        query = '''
            DROP TABLE IF EXISTS {};
            
        '''
        cur.execute(sql.SQL(query).format(sql.Identifier(tbl_query)))
        conn.commit()


def create_agency_tbl():
    agency = ''' 
        CREATE TABLE gtfs_translink_agency (
            agency_url      VARCHAR(511),
            agency_name     VARCHAR(255),
            agency_timezone VARCHAR(255),
            agency_id       VARCHAR(15),
            agency_lang     VARCHAR(15)
        );
    '''
    cur.execute(agency)
    conn.commit()

def create_calendar_tbl():
    calendar = '''
        CREATE TABLE gtfs_translink_calendar (
            service_id VARCHAR(127),
            start_date INTEGER,
            end_date   INTEGER,
            monday     INTEGER,
            tuesday    INTEGER,
            wednesday  INTEGER,
            thursday   INTEGER,
            friday     INTEGER,
            saturday   INTEGER,
            sunday     INTEGER
            
        );
    '''
    cur.execute(calendar)
    conn.commit()


def create_calendar_dates_tbl():
    calendar_dates = '''
        CREATE TABLE gtfs_translink_calendar_dates (
            service_id     VARCHAR(127),
            date           INTEGER,
            exception_type INTEGER
        );
    '''
    cur.execute(calendar_dates)
    conn.commit()

def create_routes_tbl():
    routes = '''
        CREATE TABLE gtfs_translink_routes (
            route_long_name   VARCHAR(255),
            route_type        INTEGER,
            route_text_color  VARCHAR(6),
            route_color       VARCHAR(6),
            agency_id         VARCHAR(15),
            route_id          INTEGER,
            route_url         VARCHAR(550),
            route_desc        VARCHAR(255),
            route_short_name  VARCHAR(255)
        );
    '''
    
    cur.execute(routes)
    conn.commit()

def create_shapes_tbl():
    shapes = '''
        CREATE TABLE gtfs_translink_shapes (
            shape_id            VARCHAR(8),
            shape_pt_lat        REAL,
            shape_pt_lon        REAL,
            shape_pt_sequence   INTEGER,
            shape_dist_traveled REAL
        );
    '''
    
    cur.execute(shapes)
    conn.commit()

def create_stop_times_tbl():
    stop_times = '''
        CREATE TABLE gtfs_translink_stop_times (
            trip_id             VARCHAR(63),
            arrival_time        INTERVAL,
            departure_time      INTERVAL,
            stop_id             INTEGER,
            stop_sequence       INTEGER,
            stop_headsign       INTEGER,
            pickup_type         INTEGER,
            drop_off_type       INTEGER,
            shape_dist_traveled REAL,
            timepoint           INTEGER
        );
    '''
    cur.execute(stop_times)
    conn.commit

def create_stops_tbl():
    stops = '''
        CREATE TABLE gtfs_translink_stops (
            stop_lat            REAL,
            stop_code           INTEGER,
            stop_lon            REAL,
            stop_id             VARCHAR(63),
            stop_url            VARCHAR(511),
            parent_station      VARCHAR(63),
            stop_desc           VARCHAR(255),
            stop_name           VARCHAR(255),
            location_type       INTEGER,
            zone_id             VARCHAR(50)
        );
    '''
    cur.execute(stops)
    conn.commit()

def create_trips_tbl():
    trips = '''
        CREATE TABLE gtfs_translink_trips (
            block_id              VARCHAR(20),
            bikes_allowed         INTEGER,
            route_id              INTEGER,
            wheelchair_accessible INTEGER,
            direction_id          INTEGER,
            trip_headsign         VARCHAR(255),
            shape_id              VARCHAR(8),
            service_id            VARCHAR(63),
            trip_id               VARCHAR(63),
            trip_short_name       VARCHAR(63)
            
        );
    '''
    cur.execute(trips)
    conn.commit()

def create_stop_order_exceptions_tbl():
    stop_order_exceptions = '''
        CREATE TABLE gtfs_translink_stop_order_exceptions (
            route_name           VARCHAR(63),
            direction_name       VARCHAR(63),
            direction_do         VARCHAR(124),
            stop_id              INTEGER,
            stop_name            VARCHAR(124),
            stop_do              INTEGER
        );
    '''
    cur.execute(stop_order_exceptions)
    conn.commit()

def create_direction_names_exceptions_tbl():
    direction_names_exceptions= '''
        CREATE TABLE gtfs_translink_direction_names_exceptions(
            route_name              VARCHAR(50),
            direction_id            INTEGER,
            direction_name          VARCHAR(50),
            direction_do            INTEGER
            );
    '''
    cur.execute(direction_names_exceptions)
    conn.commit()

def create_directions_tbl():
    directions = '''
        CREATE TABLE gtfs_translink_directions(
            direction           VARCHAR(10),
            direction_id        INTEGER,
            route_id            INTEGER,
            route_short_name    VARCHAR(50) 
            );
            
    '''
    cur.execute(directions)
    conn.commit()

def run():
    drop_tables()
    create_agency_tbl()
    create_calendar_tbl()
    create_calendar_dates_tbl()
    create_routes_tbl()
    create_shapes_tbl()
    create_stop_times_tbl()
    create_stops_tbl()
    create_trips_tbl()
    create_stop_order_exceptions_tbl()
    create_directions_tbl()
    create_direction_names_exceptions_tbl()
if __name__ == '__main__':
    run()
