#!/usr/bin/env python
DATABASE_HOST = 'localhost'
DATABASE_PORT = 11111
DATABASE_USER = 'postgres'
DATABASE_PASSWORD = 'postgres'
DATABASE_DBNAME = 'data-prep-realtime-gtfs'
