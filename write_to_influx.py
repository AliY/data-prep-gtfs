import requests
import json
from datetime import datetime
from time import sleep
import os
from datetime import date, datetime
from time import mktime

from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client import InfluxDBClient, Point, WriteOptions
from influxdb_client.domain.write_precision import WritePrecision

from google.protobuf.json_format import MessageToDict
from google.transit import gtfs_realtime_pb2
from google.protobuf.json_format import MessageToJson

Write_Precision = WritePrecision.S
# Translink API tokens
transit_api_tokens = ['IZjFu6LJDgEJ6b0wqKj9','aCaEZutHoyTz0kkGGh77', 'JUp457WI2xXlrxk0YrFL','ow1SIMs32a9lbyP6WQgv', 'oF6itkxvEKTc6xRDA9Dv', 'jE1KxH0LHxVdOEzG8e1G','P6L02EApifRPTGW6C9RU',
        'qqpqedgISACX47MRr38N', 'PazSzKxDJCu2OntqZonS']
#three urls for each realtime gtfs update (tripUpdate,vehiclePoisition, alerts)
trip_api_url = "https://gtfs.translink.ca/v2/gtfsrealtime?apikey={}".format(transit_api_tokens[0])
vehicle_api_url = "https://gtfs.translink.ca/v2/gtfsposition?apikey={}".format(transit_api_tokens[1])
alert_api_url= "https://gtfs.translink.ca/v2/gtfsalerts?apikey={}".format(transit_api_tokens[2])

# InfluxDB token, org & bucket name
token = "XdCulrUhacg_b9qv6fhAqh3POvNakq0Jn4HC26l_-l6eSSgCaUKj729R7ywG4LGzgPjuYdy9OK66r6DQ0QYL7w=="
organization = "HM"
bucket = "RealTimeGTFS"
# Store the URL of your InfluxDB instance
url="http://localhost:8086/"


client = InfluxDBClient(url=url, token=token, org = organization)

write_api = client.write_api(write_options=SYNCHRONOUS)
query_api = client.query_api()


def write_tripUpdate(trip_api_url):
    response = requests.get(trip_api_url)
    feed = gtfs_realtime_pb2.FeedMessage()
    feed.ParseFromString(response.content)

    for ent in feed.entity:
        trip_id = ent.trip_update.trip.trip_id
        route_id = ent.trip_update.trip.route_id
        direction_id = ent.trip_update.trip.direction_id
        
        vehicle_id = ent.trip_update.vehicle.id
        vehicle_label = ent.trip_update.vehicle.label
        
        for stop in ent.trip_update.stop_time_update:
            stop_id = stop.stop_id
            stop_sequence = stop.stop_sequence
            arrival_delay = stop.arrival.delay
            arrival_time = stop.arrival.time
            departure_delay = stop.departure.delay
            departure_time = stop.departure.time
        
        point_dict ={
            "measurement": "TranslinkTripUpdate",
            "tags": {
                "trip_id": str(trip_id),
                "stop_id" : str(stop_id),
                "route_id": str(route_id),
                "direction_id": str(direction_id),
                "vehicle_id" : str(vehicle_id),
                "vehicle_label" : str(vehicle_label),
                "stop_id" : str(stop_sequence),
            },
            "fields": {
                "arrival_delay": arrival_delay,
                "departure_delay" : departure_delay,
                "departure_time" : departure_time,
                "arrival_time" : arrival_time

            },
            "time": arrival_time
        }
        p = Point.from_dict(point_dict,write_precision=Write_Precision)
        write_api.write(bucket=bucket, record=p)

def write_vehiclePosition(vehicle_api_url):
    response = requests.get(vehicle_api_url)
    feed = gtfs_realtime_pb2.FeedMessage()
    feed.ParseFromString(response.content)

    for ent in feed.entity:
        vehicle_dict = {
            "measurement" : "TranslinkVehicleUpdate",
            "tags" : {
                "id" : ent.id,
                "trip_id" : ent.vehicle.trip.trip_id,
                "start_date" : ent.vehicle.trip.start_date,
                "route_id" : ent.vehicle.trip.route_id,
                "direction_id" : ent.vehicle.trip.direction_id,
                "stop_id" : ent.vehicle.stop_id,
                "vehicle_id" : ent.vehicle.vehicle.id,
                "vehicle_label" : ent.vehicle.vehicle.label
            },
            "fields" : {
                "current_stop_sequence" : ent.vehicle.current_stop_sequence,
                "latitude" : ent.vehicle.position.latitude,
                "longitude" : ent.vehicle.position.longitude,
            },
            "time" : ent.vehicle.timestamp

        }
        #write point to influxdb
        p = Point.from_dict(vehicle_dict,write_precision=Write_Precision)
        write_api.write(bucket=bucket, record=p)

def write_alert(alert_api_url):
    response = requests.get(alert_api_url)
    feed = gtfs_realtime_pb2.FeedMessage()
    feed.ParseFromString(response.content)
    for ent in feed.entity:        
        alerts_dict = {
            "measurement" : "TranslinkAlerts",
            "tags" : {
                "id" : ent.id,
                "agency_id" : ent.alert.informed_entity[0].agency_id,
                "route_id" : ent.alert.informed_entity[0].route_id,
                "cause" : ent.alert.cause,
                "header_text" : ent.alert.header_text.translation[0].text,
                "description_text" : ent.alert.description_text.translation[0].text
            },
            "fields" : {
                "route_type" : ent.alert.informed_entity[0].route_type,
                "severity_level" : ent.alert.severity_level
            },
            "time" : ent.alert.active_period[0].start,

        }
        #write point to influxdb
        p = Point.from_dict(alerts_dict,write_precision=Write_Precision)
        write_api.write(bucket=bucket, record=p)

def send_query():
    # using Table structure
    tables = query_api.query(
        'from(bucket:"RealTimeGTFS") |> range(start: -10m) |> '
        )

    for table in tables:
        print(table)
        for row in table.records:
            print (row.values)

def run():
    write_tripUpdate(trip_api_url)
    write_vehiclePosition(vehicle_api_url)
    write_alert(alert_api_url)
    # send_query()
    
if __name__ == "__main__":
    run()